package de.peralty.udemyrest

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class UdemyRestApplication

fun main(args: Array<String>) {
	runApplication<UdemyRestApplication>(*args)
}
